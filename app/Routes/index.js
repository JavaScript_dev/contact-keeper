const users = require("./users");
const contact = require("./contact");
const auth = require("./auth");

module.exports = (app) => {
  app.use("/api/users", users);
  app.use("/api/contact", contact);
  app.use("/api/auth", auth);
};
